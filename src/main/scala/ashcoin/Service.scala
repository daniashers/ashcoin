package ashcoin

import io.circe._
import org.http4s._
import org.http4s.circe._
import org.http4s.server._
import org.http4s.dsl._

import scala.io.Source
import scalaz.concurrent.Task
import org.http4s.headers.`Content-Type`

import scala.util.Try


object Service {

  var chain: Chain[String,Int,Md5] = {
    val genesisBlock = EzBlock(previousHash = Md5(Array.fill(16)(0.toByte)), minedBy = "nobody", payload = "Genesis", nonce = 0)
    EzChain.seed[String,Int,Md5](genesisBlock)
  }

  def blockLabel[A, N, H](block: Block[A, N, H]): String = {
    s"""
      |${block.hash}
      |Mined by: ${block.minedBy}
      |Payload: ${block.payload.toString}
      |Nonce: ${block.nonce.toString}
    """.stripMargin.trim.replace("\n", "\\n").replace("\"", "\\\"")
  }

  val service = HttpService {

    case GET -> Root / "chain" =>

      val mainChain = chain.longest

      val allNodesString = chain.allNodes.map { node =>
        val appearance = if (mainChain.exists(_.hash == node.hash)) "style=filled fillcolor=lightblue" else ""
        s""""${node.hash.asHex}"[label="${blockLabel(node)}" URL="newBlockForm/${node.hash.asHex}" $appearance]"""
      }.mkString("\n")

      val allEdgesString = chain.edges.map { case (frm, to) =>
        s""""${frm.asHex}" -> "${to.asHex}";"""
      }.mkString("\n")

      val response =
        s"""
          | digraph g {
          |   node [shape=rectangle fontsize=8 fontname=courier];
          |   $allNodesString
          |   $allEdgesString
          | }
          |
        """.stripMargin.trim
      Ok(response)

    case request @ GET -> Root =>
      StaticFile.fromResource("/chain.html", Some(request)).map(Task.now).getOrElse(NotFound())

    case request @ GET -> Root / "newBlockForm" / prevHash =>
      EzTemplate("newBlockForm.html", Map("hash" -> prevHash))

    case request @ GET -> Root / "blocks" / hash / "status" =>
      Md5.fromString(hash) match {
        case None => NotFound(s"Block not found: $hash")
        case Some(md5) =>
          val longest = chain.longest
          val isOnMainLine = longest.exists(_.hash == md5)
          val isLeaf = chain.isLeaf(md5)
          val json = s"""{ "isOnMainLine": $isOnMainLine, "isLeaf": $isLeaf }"""
          Ok(json).putHeaders(`Content-Type`(MediaType.`application/json`))
      }

    case request @ POST -> Root / "submitBlock" =>
      val u = request.as[UrlForm]
      u.flatMap { form =>
        (for {
          prevHashStr <- form.getFirst("prevHash").map(_.trim)
          prevHash    <- Md5.fromString(prevHashStr)
          miner       <- form.getFirst("miner").map(_.trim)
          payload     <- form.getFirst("payload").map(_.trim)
          nonceStr    <- form.getFirst("nonce").map(_.trim)
          nonce       <- Try(Integer.parseInt(nonceStr)).toOption
        } yield {
          val block = EzBlock(prevHash, miner, payload, nonce)
          chain.addBlock(block) match {
            case Left(error) =>
              EzTemplate("submitFailure.html", Map("reason" -> error))
            case Right(newChain) =>
              chain = newChain
              EzTemplate("submitSuccess.html", Map("hash" -> block.hash.asHex))
          }
        }).getOrElse(BadRequest("Invalid data has been entered. Please check all formatting."))
      }

    case request @ GET -> Root / file =>
      StaticFile.fromResource("/" + file, Some(request)).map(Task.now).getOrElse(NotFound())

  }
}

// A quick and dirty template engine...
object EzTemplate {
  def apply(resourceName: String, values: Map[String, String]): Task[Response] = {
    import org.http4s.headers._
    val fileContents = Source.fromResource(resourceName).getLines.toList.mkString("\n")
    val responseBody = values.foldLeft(fileContents) { case (contents, (key, value)) =>
      fileContents.replace(s"{{$key}}", value)
    }
    Ok(responseBody).putHeaders(`Content-Type`(MediaType.`text/html`))
  }
}