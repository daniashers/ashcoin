package ashcoin

trait Chain[A, N, H] {
  def longest: Seq[Block[A, N, H]]
  def allNodes: Seq[Block[A, N, H]]
  def edges: Seq[(H, H)]
  def isLeaf(hash: H): Boolean
  def addBlock(block: Block[A, N, H]): Either[String, Chain[A, N, H]]
}


case class EzChain[A, N, H] private(nodes: Seq[Block[A,N,H]], edges: Seq[(H, H)], root: H) extends Chain[A, N, H] {

  private def successorsOf(hash: H): Seq[H] = {
    edges.collect { case (frm, to) if frm == hash => to }
  }

  // Unsafe!
  // But as long as we have used the provided interface (main constructor is private) it shouldn't fail.
  private def node(hash: H): Block[A, N, H] = nodes.find(_.hash == hash).get

  lazy val longest: Seq[Block[A, N, H]] = {
    def go(hash: H): Seq[Block[A, N, H]] = {
      if (successorsOf(hash).isEmpty) Seq(node(hash))
      else node(hash) +: successorsOf(hash).map(go(_)).maxBy(_.length)
    }
    go(root)
  }

  def allNodes: Seq[Block[A, N, H]] = nodes

  def isLeaf(hash: H): Boolean = nodes.exists(_.hash == hash) && !edges.exists{ case (frm, to) => frm == hash }

  def addBlock(newBlock: Block[A, N, H]): Either[String, EzChain[A, N, H]] = {
    if (!newBlock.isValid) Left("Block is not valid")
    else nodes.find(_.hash == newBlock.previousHash) match {
      case None => Left("Previous block not found")
      case Some(blockNode) =>
        Right(
          this.copy(
            nodes = this.nodes :+ newBlock,
            edges = this.edges :+ (newBlock.previousHash -> newBlock.hash)
          )
        )
    }
  }
}

object EzChain {
  def seed[A,N,H](seedBlock: Block[A,N,H]) = EzChain(Seq(seedBlock), Nil, seedBlock.hash)
}