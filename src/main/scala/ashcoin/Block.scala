package ashcoin

trait Block[A, N, H] {
  val previousHash: H
  val minedBy: String
  val payload: A
  val nonce: N
  val hash: H
  def isValid: Boolean
}

case class EzBlock(previousHash: Md5, minedBy: String, payload: String, nonce: Int) extends Block[String, Int, Md5] {
  lazy val hash: Md5 = Md5.of(previousHash.asHex + minedBy.toString + payload.toString + nonce.toString)
  override def isValid: Boolean = {
    def indexOfFirstLetter(msg: String): Int = msg.toUpperCase.find(_.isLetter).map(_.toInt - 64).getOrElse(0)
    val lastDigitOfPreviousHash = Integer.parseInt(previousHash.asHex.takeRight(1), 16)
    (lastDigitOfPreviousHash + indexOfFirstLetter(minedBy) + indexOfFirstLetter(payload) + nonce) % 10 == 0
  }
}
