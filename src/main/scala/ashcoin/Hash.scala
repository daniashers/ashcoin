package ashcoin

import scala.util.Try

case class Md5(value: Seq[Byte]) {
  def asHex: String = value.map("%02x".format(_)).mkString("")
  override def toString: String = asHex
}

object Md5 {
  def of(value: String): Md5 =
    Md5(java.security.MessageDigest.getInstance("MD5").digest(value.toCharArray.map(_.toByte)))
  def fromString(str: String): Option[Md5] = {
    if (str.length != 32) None
    else {
      val bytesOpt = Try {
        str.grouped(2).map(byteRepr => Integer.parseInt(byteRepr, 16).toByte).toSeq
      }.toOption
      bytesOpt.map(byteSeq => Md5(byteSeq))
    }
  }
}
