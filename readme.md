#Ashcoin
##A toy implementation of a blockchain

#####Instructions
- Start the app and open your browser at localhost:8080
- You can mine a successor to any of the nodes displayed by clicking on it
- The payload can be any string (no currency accounting takes place!)
- The main (longest) chain will be highlighted

The 'proof of work' is designed for a live demonstration and so is easy enough to be done mentally: The last digit of the previous hash (converted to decimal e.g. b = 11) plus the index of the first letter of the miner (e.g. 'Bob' B = 2) plus the index of the first letter of the payload plus the nonce must be divisible by 10.

Example:
Hash of previous block: ...68ef -- F = 15
Mined by: Roger -- R = 18
Payload: Hello -- H = 8

15 + 18 + 8 = 41 -- to make it divisible by ten we must add a nonce of 9. 

This is a very weak proof of work obviously.
It can be changed easily in the code.

Note:
MD5 is used as the hashing function, it is probably not secure enough but this is just a demo.
